#include <iostream>
#include <string>
#include <thread>

#include <bobcat/fileclock> 
#include <bobcat/highresolutionclock> 
#include <bobcat/steadyclock> 
#include <bobcat/systemclock> 

using namespace std;
using namespace FBB;

int main()
{
    SystemClock sysNow{  };
    cout << "system clock at " << sysNow    << "\n"
            "elapsed: " << sysNow.elapsed() << "\n\n";

                                    // same timespec, elapsed ns.
    FileClock fileNow{ sysNow };    // is clock-specific
    cout << "file clock at " << fileNow << "\n"
            "elapsed: " << fileNow.elapsed() << "\n\n";

    SystemClock sysNow2{ fileNow };
    cout << "system clock at " << sysNow2 << "\n"
            "elapsed: " << sysNow2.elapsed() << "\n\n";

    cout << sysNow2("%Y %b %d, %H:%M:%S")           << "\n"
            "\n"
            "minimum time: " << sysNow2.min()       << "\n"
            "maximum time: " << SystemClock::max()  << "\n\n";

    // conversion to less precise time specification:
    cout << "100 minutes is " << toDouble<hours>(100min) << " hours\n\n";

    HighResolutionClock hrc{ fileNow };
    cout << "high resolution clock at " << hrc << "\n\n";

    SteadyClock sc0;            // computing 'since' itself takes several
    auto passed = since(sc0);   // (variable) hundred nano seconds
    cout << "sc0 since: " << passed << '\n';

    SteadyClock sc;

    this_thread::sleep_for(1s);

    cout << 
        "ELAPSED:  " << since(sc) << '\n' <<
        "(small delay...)\n"
        "as count: " << countSince(sc) << "\n\n";
}



