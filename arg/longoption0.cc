#include "arg.ih"

LongOption__::LongOption__(char const *name)
:
    d_name(name),
    d_type(NoArg),
    d_optionChar(0)
{}
