#include "filesystem.ih"

//DEFAULT     
//NEW, REPLACE, UPDATE    - only one
//RECURSIVE               - not with FILE. Not RECURSIVE then 1 level deep
//
//CP_SYMLINKS             - with copy(): cp symlinks as symlinks
//SKIP_SYMLINKS           - with copy: skip symlinks
//DIRS_ONLY              - with copy: only copy the dir. structure
//
//SYMLINK                 create_symlink as in 'ln -s d_path dest'
//                        if d_path is a dir: create_directory_symlink
//
//LINK                    hard link of src/dest
//
//    FILE                    calls copy_file
//
//    SYMLINK_CP           - calls copy_symlink: both must be symlinks
//
//
//options:
//    & FILE          - copy_file
//                        | DEFAULT NEW, REPLACE, UPDATE
//
//    & CP_SYMLINK    - copy_symlink,     no further options
 

bool FileSystem::copy(Path const &dest, FSOptions options)
{
    switch (options & (NEW_LINK | NEW_SYMLINK | CP_SYMLINK | FILE))
    {
        case NEW_LINK:
        return hardLink(dest);

        case NEW_SYMLINK:
        return symLink(dest);

        case CP_SYMLINK:
        return cpSymLink(dest);

        case FILE:                          // only files
        return copyFile(dest, options);

        default:                            // also recursive
        return fsCopy(dest, options);
    }
}


