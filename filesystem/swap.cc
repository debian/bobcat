#include "filesystem.ih"

void FileSystem::swap(FileSystem &other)
{
    std::swap(d_ec, other.d_ec);
    d_path.swap(other.d_path);
}
