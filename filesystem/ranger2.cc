#include "filesystem.ih"

Ranger<FileSystem::RecursiveIter> FileSystem::recursiveRange() const
{
    return
        d_ec == 0 ?
            Ranger<RecursiveIter>(
                    fs::recursive_directory_iterator{ d_path }, 
                    fs::recursive_directory_iterator{}
            )
        :
            Ranger<RecursiveIter>(
                    fs::recursive_directory_iterator{ d_path, *d_ec }, 
                    fs::recursive_directory_iterator{}
            );
}

