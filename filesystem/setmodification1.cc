#include "filesystem.ih"

bool FileSystem::setModification(SystemClock::time_point const &time)
{
    FileClock::time_point fileTime = clock_cast<FileClock>(time);

    if (d_ec == 0)
    {
        fs::last_write_time(d_path, fileTime);
        return true;
    }

    fs::last_write_time(d_path, fileTime, *d_ec);
    return static_cast<bool>(*d_ec);
}


