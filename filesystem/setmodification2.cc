#include "filesystem.ih"

bool FileSystem::setModification(DateTime const &time)
{
    return setModification( SystemClock::from_time_t(time.utcSeconds()) );
}
