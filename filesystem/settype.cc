#include "filesystem.ih"

bool FileSystem::setType(FileType type, bool destination)
{
    status(destination).type(type);
    return status(destination).type() == type;  // check the current status
}
