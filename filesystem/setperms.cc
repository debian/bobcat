#include "filesystem.ih"

FileSystem &FileSystem::setPerms(Perms perms, FSOptions opt) const
{
    if (d_ec == 0)
        fs::permissions(d_path, perms, static_cast<PermOpts>(opt));
    else
        fs::permissions(d_path, perms, static_cast<PermOpts>(opt), *d_ec);

    return const_cast<FileSystem &>(*this);
}
