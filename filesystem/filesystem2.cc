#include "filesystem.ih"

FileSystem::FileSystem(Path const &path, EC &ec)
:
    d_ec(&ec),
    d_path(path)
{}
