#include "filesystem.ih"

Ranger<FileSystem::DirIter> FileSystem::dirRange() const
{
    return
        d_ec == 0 ?
            Ranger<DirIter>(fs::directory_iterator{ d_path }, 
                            fs::directory_iterator{})
        :
            Ranger<DirIter>(fs::directory_iterator{ d_path, *d_ec }, 
                            fs::directory_iterator{});
}

