#include "filesystem.ih"

bool FileSystem::optEC2( Path const &dest,
                        bool (*fun1)(Path const &, Path const &),
                        bool (*fun2)(Path const &, Path const &, EC &)) const
{
    return d_ec != 0 ?
                (*fun2)(d_path, dest, *d_ec)
            :
                fun1(d_path, dest);
}

