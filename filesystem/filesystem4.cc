#include "filesystem.ih"

FileSystem::FileSystem(Path &&tmp, EC &ec)
:
    d_ec(&ec),
    d_path(move(tmp))
{}
