#include "filesystem.ih"

bool FileSystem::copyFile(Path const &dest, unsigned options) const
{
    fs::copy_options opts = 
                static_cast<CopyOpts>(options &= (FILE - 1));

    return d_ec == 0 ?
                fs::copy_file(d_path, dest, opts)
            :
                fs::copy_file(d_path, dest, opts, *d_ec);
}
