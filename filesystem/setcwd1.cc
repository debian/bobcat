#include "filesystem.ih"

FileSystem &FileSystem::setCwd()
{
    optEC1(fs::current_path, fs::current_path);
    return *this;
}
