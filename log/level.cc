#include "log.ih"

ostream &Log::level(size_t msgLevel)
{
    //              levelOK            opfunOK
    *d_active = { d_level <= msgLevel, false    };

    // d_active->levelOK = d_level <= msgLevel;
    // d_active->opfunOK = false;

                                        // LogBuf's member: maybe activates
    setActive(d_active->levelOK);       //                  logging

    return *this;
}
