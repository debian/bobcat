#include "log.ih"

Log &Log::initialize(std::string const &filename,
                    std::ios::openmode mode, char const *delim)
{
    if (s_stream.get())         // multiple initializations are not allowed
        throw Exception{} <<
                "Log::initialize: FBB::Log already initialized";
                                // initialize the static s_stream
    s_stream.reset(new Log(filename, mode, delim));

    return *s_stream.get();     // and return the initialized Log object
}
