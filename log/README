enum LogManipulator
{
    FATAL,
    nl,                 // new line without new time stamp
    fnl,                // forced new line, even at low level specs.
};

class Log: private LogBuf, public std::ostream
{
                                                                    // opins:
    friend Log &operator<<(Log &log, LogManipulator manip);         //  2.cc
    friend Log &operator<<(Log &log,                                //  3.cc
                           std::ostream &(*fun)(std::ostream &str)); 
    friend Log &operator<<(Log &log,                                //  4.cc
                           std::ios_base &(*fun)(std::ios_base &base));
    template<typename Type>
    friend Log &operator<<(Log &log, Type const &type);             //   .f

    std::ofstream d_stream;

    size_t d_level;         // defined by setLevel: the minimum level 
                            // messages must have to be inserted: if d_level
                            // <= the actually specified msg level the message
                            // is inserted
    struct Active
    {
        bool levelOK;       // if 'true' next inserted message are logged
        bool opfunOK;

                            // log characters accepted by opfun.cc (?)
        std::string accept; // accepted log characters for opfun.cc
    };
    std::unique_ptr<Active> d_active;

                                                // Log object used by 
    static std::unique_ptr<Log> s_stream;       // initialize() and instance()

    public:
                    // initializes s_stream, returns its Log object
        static Log &initialize(std::string const &filename,
                std::ios::openmode = std::ios::out | std::ios::app,
                char const *delim = " ");

                    // returns s_stream's Log object
        static Log &instance();

            // defines the base class objects, calls init()
        Log();                                                  // 1.cc

            // defines the base class objects, calls init()
        Log(std::ostream &out,  char const *delim = " ");       // 2.cc

            // forwards to open(), also called by initialize()
        Log(std::string const &filename,                        // 3.cc
                std::ios::openmode = std::ios::out | std::ios::app,
                char const *delim = " ");

            // redefines LogBuf, calls init()
        void open(std::string const &filename,
                std::ios::openmode = std::ios::out | std::ios::app,
                char const *delim = " ");

            // returns the current message level (i.e., d_level)
        size_t level() const;                                           // .f

            // updates d_active's levelOK, opfunOK to false
            // messages are logged if levelOK is true
// WIP
        std::ostream &level(size_t useLevel);                           // .cc

            // d_level = newLevel, activates msg logging at 'newLevel'
        void  setLevel(size_t newLevel);

        using LogBuf::setTimeStamp;
// OBS  void  setTimestamp(TimeStamps timeStamp, char const *delim = " ");

            // logging completely off/on, but on() can specify a level nr.
        void  off();                                                    // .f
        void  on(size_t logLevel = 0);

        Log &operator()(char accept);

        void str(std::string const &str);
        std::string const &str() const;

    private:
        void init();
};

    // Not in FBB, but overloading the std operator<<(ostream &) function:
std::ostream &operator<<(std::ostream &str, FBB::LogManipulator); // opins1.cc

#endif
